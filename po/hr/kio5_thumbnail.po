# Translation of kio_thumbnail to Croatian
#
# Marko Dimjasevic <marko@dimjasevic.net>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: kio_thumbnail\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-10 00:46+0000\n"
"PO-Revision-Date: 2011-03-04 19:17+0100\n"
"Last-Translator: Marko Dimjasevic <marko@dimjasevic.net>\n"
"Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: thumbnail.cpp:194
#, kde-format
msgid "No MIME Type specified."
msgstr "Nije određen MIME tip."

#: thumbnail.cpp:201
#, kde-format
msgid "No or invalid size specified."
msgstr "Nema ili je navedena kriva veličina."

#: thumbnail.cpp:224
#, kde-format
msgid "Cannot create thumbnail for directory"
msgstr "Ne mogu stvoriti sličicu za direktorij"

#: thumbnail.cpp:235
#, kde-format
msgid "No plugin specified."
msgstr "Nije naveden priključak."

#: thumbnail.cpp:240
#, kde-format
msgid "Cannot load ThumbCreator %1"
msgstr "Ne mogu učitati ThumbCreator %1"

#: thumbnail.cpp:257
#, kde-format
msgid "Cannot create thumbnail for %1"
msgstr "Ne mogu stvoriti sličicu za %1"

#: thumbnail.cpp:269
#, kde-format
msgid "Failed to create a thumbnail."
msgstr "Neuspjelo stvaranje sličice."

#: thumbnail.cpp:280
#, kde-format
msgid "Could not write image."
msgstr "Ne mogu zapisati sliku."

#: thumbnail.cpp:304
#, kde-format
msgid "Failed to attach to shared memory segment %1"
msgstr "Neuspjelo pridavanje dijeljenog memorijskog segmenta %1"

#: thumbnail.cpp:311
#, kde-format
msgid "Image is too big for the shared memory segment"
msgstr "Slika je prevelika za dijeljeni memorijski segment"

#~ msgctxt "@option:check"
#~ msgid "Rotate the image automatically"
#~ msgstr "Automatski zakreći sliku"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Žarko Pintar"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "zarko.pintar@gmail.com"

#~ msgid "kio_thumbmail"
#~ msgstr "kio_thumbmail"
